#!/usr/bin/make -f
# -*- makefile -*-

# get octave paths (have to build-depend on octave-headers)
include /usr/share/octave/debian/defs.make
# same for Matlab
include /usr/share/matlab/debian/defs.make

# on older systems such as 16.04 PIE is not enabled by  default and
# configure them fails # to link
DISABLE_HARDENING_OPTS=$(shell  g++ -v 2>&1 | grep -q -e --enable-default-pie || echo ",-pie")
export DEB_BUILD_MAINT_OPTIONS=hardening=+all$(DISABLE_HARDENING_OPTS)

# To avoid --match=native since wouldn't be supported on some platforms
export CPP_MARCH=

include /usr/share/dpkg/architecture.mk
# guarantee the same precision on x86 platforms for tests
ifneq (,$(findstring z$(DEB_HOST_ARCH)z,zamd64z zi386z))
    CFLAGS+=-mmmx -msse -msse2 -mfpmath=sse
endif

# To prevent any online interactions
export  http_proxy=http://127.0.0.1:9/
export  https_proxy=http://127.0.0.1:9/

export QT_SELECT=qt5

%:
	dh $@

override_dh_auto_configure:
	./configure -openmp # -debug

override_dh_auto_build:
	./build -verbose

override_dh_install:
	dh_install
	# Remove __pycache__ dirs that were created in build time tests
	find debian -name __pycache__ -type d | xargs rm -rf

override_dh_auto_test:
# Disabled for now since require test data which is linked
# via git submodules in original upstream github
#ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
#	./run_tests || { cat testing.log; exit 1; }
#endif

override_dh_link:
	cp debian/mrtrix3.links.in debian/mrtrix3.links
	: # Generate symlinks for all the mr* commands
	for f in debian/mrtrix3/usr/lib/mrtrix3/bin/mrview; do \
		bn=$$(basename $$f); \
		echo "usr/lib/mrtrix3/bin/$$bn usr/bin/$$bn" >> debian/mrtrix3.links; \
	    echo "/usr/share/man/man1/mrtrix3.1.gz /usr/share/man/man1/$$bn.1.gz" >> debian/mrtrix3.links; \
	done
	echo "/usr/share/mrtrix3/matlab $(MATLAB_MDIR)/mrtrix" >> debian/mrtrix3.links
	echo "/usr/share/mrtrix3/matlab $(MDIR)/mrtrix" >> debian/mrtrix3.links
	dh_link

# we want immediately useable docs and example data, they are in a dedicated
# package already
override_dh_compress:
	dh_compress -X.html

override_dh_clean:
	./build clean
	find . -name '*.pyc' -delete
	-rm clean \
		config configure.log build.log \
		core/version.cpp lib/mrtrix3/_version.py src/exec_version.cpp
	dh_clean
	@rm -f debian/mrtrix.links

override_dh_shlibdeps:
	dh_shlibdeps -l $(CURDIR)/lib

override_dh_python3:
	dh_python3 --shebang=/usr/bin/python3
